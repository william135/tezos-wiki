---
layout: post
title:  "Tezos Projects"
date:   2019-01-07 12:14:18
---
# Tezos Projects {#projects}

## Tokenization

### BTG Pactual
In May 2020, [BTG Pactual](https://www.btgpactual.com/)—Latin America’s largest standalone investment bank—formally announced the launch of ReitBZ, a USD ~1B REIT offered as natively digital (tokenized) securities on the Tezos blockchain. This real-estate backed token issuance marked the beginning of BTG’s commitment to utilize Tezos for all of its future digital securities offerings—a pivot from its previous Ethereum-based strategy.

### Elevated Returns
[Elevated Returns](https://www.elevatedreturns.com/)—the digital asset management company responsible for tokenizing the St. Regis Resort in Aspen, Colorado—selected Tezos as the default blockchain for its real-estate backed digital securities offerings. Elevated Returns began its transition onto Tezos with the migration of AspenCoin in February 2019. The company has since launched its own digital securities exchange, on which it is expected to offer over $1B in tokenized real estate assets on Tezos. 

### Inveniam
[Inveniam](https://inveniam.io/) is a data integrity platform for private markets that facilitates price discovery and liquidity for private equity and commercial real estate assets. Inveniam uses the Tezos blockchain as the backbone of its data integrity offering.

### Logical Pictures

### Sustain Exchange

### Vertalo
In January 2020, [Vertalo](https://www.vertalo.com/)—an industry-leading platform for digital asset lifecycle management—selected Tezos as its primary blockchain for digital securities issuance and lifecycle management. Also a regulated digital transfer agent, Vertalo has since announced over ~$200M in digital securities offerings from 20 different companies, all of which will be launched on Tezos.

In January 2020, [Vertalo](https://www.vertalo.com)—an industry-leading platform for digital asset lifecycle management—selected Tezos as its primary blockchain for digital securities issuance and lifecycle management. Also a regulated digital transfer agent, Vertalo has since announced over ~$200M in digital securities offerings from 20 different companies, all of which will be launched on Tezos.

## Gaming / NFTs / Creator

### Kalamint
Launching in February 2021, [Kalamint](https://kalamint.io/) is the first non-fungible token (NFT) marketplace on the Tezos blockchain. Kalamint V1 enables users to mint NFTs using Tezos’s FA2 token standard and subsequently buy, sell and collect these digital assets on the platform. 

### OpenMinter

### Crictez

## Decentralized Finance (DeFi)

### tzBTC

### Decentralized Exchanges

#### Dexter

#### Quipuswap

### Stablecoins

#### Checker

#### USDS (Stably)

#### Kolibri

#### USDtz

### Wrap Protocol (Bender Labs)

### Stakepool

### StakerDAO

### Oracles

#### Harbinger

#### Chainlink

## Enterprise


### Werenode

### Ecoo
